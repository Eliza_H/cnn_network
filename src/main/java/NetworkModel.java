import org.datavec.api.io.filters.BalancedPathFilter;
import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.api.split.InputSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.datavec.image.transform.FlipImageTransform;
import org.datavec.image.transform.ImageTransform;
import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.earlystopping.EarlyStoppingConfiguration;
import org.deeplearning4j.earlystopping.EarlyStoppingModelSaver;
import org.deeplearning4j.earlystopping.EarlyStoppingResult;
import org.deeplearning4j.earlystopping.saver.InMemoryModelSaver;
import org.deeplearning4j.earlystopping.scorecalc.DataSetLossCalculator;
import org.deeplearning4j.earlystopping.termination.MaxEpochsTerminationCondition;
import org.deeplearning4j.earlystopping.trainer.EarlyStoppingTrainer;
import org.deeplearning4j.earlystopping.trainer.IEarlyStoppingTrainer;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.evaluation.classification.Evaluation;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.FileSystems;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.datavec.api.records.reader.impl.regex.RegexSequenceRecordReader.LOG;

public class NetworkModel {
    private static final Logger log = LoggerFactory.getLogger(NetworkModel.class);

    private static int batchSize = 32;
    private static long seed = 42;
    private static Random rndSeed = new Random(seed);
    private static int nEpochs = 1;
    private static double splitTrainTest = 0.8;
    private static double learningRate = 1e-3;
    private int numLabels;
    private static int height = 96;
    private static int width = 96;
    private static int channels = 3;
    private static Integer[] inputShape = {height, width, channels};

    public void execute(String args[]) throws Exception {

        log.info("Loading data....");
        nu.pattern.OpenCV.loadLocally();

        String datasetMainPath = FileSystems.getDefault().getPath("src/main/resources/dataset").toAbsolutePath().toString();
        String modelOutputDir = FileSystems.getDefault().getPath("src/main/resources/").toAbsolutePath().toString();
        new File(modelOutputDir).mkdirs();
        String modelOutputPath = modelOutputDir + "networkmodel.bin";

        /**
         * Setting up data
         */
        ParentPathLabelGenerator labelMaker = new ParentPathLabelGenerator();
        File mainPath = new File(datasetMainPath);
        FileSplit fileSplit = new FileSplit(mainPath, NativeImageLoader.ALLOWED_FORMATS, rndSeed);
        int numExamples = Math.toIntExact(fileSplit.length());
        File[] directories = mainPath.listFiles(File::isDirectory);
        numLabels = directories.length;
        BalancedPathFilter pathFilter = new BalancedPathFilter(rndSeed, labelMaker, numExamples, numLabels, 0);

        /**
         * Split data: 80% training and 20% testing
         */
        InputSplit[] inputSplit = fileSplit.sample(pathFilter, splitTrainTest, 1 - splitTrainTest);
        InputSplit trainData = inputSplit[0];
        InputSplit testData = inputSplit[1];

        /**
         *  Create extra synthetic training data by flipping, rotating
         #  images on our data set.
         */
        ImageTransform flipTransform1 = new FlipImageTransform(rndSeed);
        ImageTransform flipTransform2 = new FlipImageTransform(new Random(42));
        List<ImageTransform> transforms = Arrays.asList(new ImageTransform[]{flipTransform1, flipTransform2});

        /**
         * Normalization
         **/
        log.info("Fitting to dataset");
        ImagePreProcessingScaler preProcessor = new ImagePreProcessingScaler(0, 1);

        /**
         * Define our network architecture:
         */
        log.info("Build model....");
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(seed)
                .updater(new Adam(learningRate))
                .list()
                .layer(0, convInit("conv_init", channels, 32))
                .layer(1, new ActivationLayer.Builder().activation(Activation.RELU).name("relu1").build())
                .layer(2, new BatchNormalization.Builder().name("norm1").build())
                .layer(3, maxPool("max_pool1", new int[]{3, 3}))
                .layer(4, new DropoutLayer.Builder().dropOut(0.25).name("drop1").build())
                .layer(5, conv3x3("conv2", 64))
                .layer(6, new ActivationLayer.Builder().activation(Activation.RELU).name("relu2").build())
                .layer(7, new BatchNormalization.Builder().name("norm2").build())
                .layer(8, conv3x3("conv3", 64))
                .layer(9, new ActivationLayer.Builder().activation(Activation.RELU).name("relu3").build())
                .layer(10, new BatchNormalization.Builder().name("norm3").build())
                .layer(11, maxPool("max_pool3", new int[]{2, 2}))
                .layer(12, new DropoutLayer.Builder().dropOut(0.25).name("drop3").build())
                .layer(13, conv3x3("conv4", 128))
                .layer(14, new ActivationLayer.Builder().activation(Activation.RELU).name("relu4").build())
                .layer(15, new BatchNormalization.Builder().name("norm4").build())
                .layer(16, conv3x3("conv5", 128))
                .layer(17, new ActivationLayer.Builder().activation(Activation.RELU).name("relu5").build())
                .layer(18, new BatchNormalization.Builder().name("norm5").build())
                .layer(19, maxPool("max_pool5", new int[]{2, 2}))
                .layer(20, new DropoutLayer.Builder().dropOut(0.25).name("drop5").build())
                .layer(21, new DenseLayer.Builder().nOut(1024).name("dense1").build())
                .layer(22, new ActivationLayer.Builder().activation(Activation.RELU).name("relu6").build())
                .layer(23, new BatchNormalization.Builder().name("norm6").build())
                .layer(24, new DropoutLayer.Builder().dropOut(0.5).name("drop6").build())
                .layer(25, new OutputLayer.Builder(LossFunctions.LossFunction.MCXENT).nOut(numLabels)
                        .activation(Activation.SOFTMAX).name("output").build())
                .setInputType(InputType.convolutional(width, height, channels))
                .build();
        MultiLayerNetwork network = new MultiLayerNetwork(conf);

        network.init();
        // Visualizing Network Training
        UIServer uiServer = UIServer.getInstance();
        StatsStorage statsStorage = new InMemoryStatsStorage();
        network.setListeners(new StatsListener(statsStorage));
        uiServer.attach(statsStorage);

        /**
         * Load data
         */
        log.info("Load data....");//
        ImageRecordReader recordReader = new ImageRecordReader(height, width, channels, labelMaker);
        DataSetIterator dataIter;
        DataSetIterator testdataIter;

        log.info("Train model....");
        // Train without transformations
        recordReader.initialize(trainData, null);
        dataIter = new RecordReaderDataSetIterator.Builder(recordReader, batchSize)
                .classification(1, numLabels)
                .build();
        preProcessor.fit(dataIter);
        dataIter.setPreProcessor(preProcessor);

        ImageRecordReader testRecordReader = new ImageRecordReader(height, width, channels, labelMaker);
        testRecordReader.initialize(testData, null);
        testdataIter = new RecordReaderDataSetIterator.Builder(testRecordReader, batchSize)
                .classification(1, numLabels)
                .build();
        preProcessor.fit(testdataIter);
        testdataIter.setPreProcessor(preProcessor);

        //early stopping training
        EarlyStoppingModelSaver<MultiLayerNetwork> saver = new InMemoryModelSaver<>();
        EarlyStoppingConfiguration<MultiLayerNetwork> esConf =
                new EarlyStoppingConfiguration.Builder<MultiLayerNetwork>()
                        .epochTerminationConditions(new MaxEpochsTerminationCondition(nEpochs))
                        .scoreCalculator(new DataSetLossCalculator(testdataIter, true))
                        .evaluateEveryNEpochs(1).modelSaver(saver).build();
        IEarlyStoppingTrainer<MultiLayerNetwork> trainer = new EarlyStoppingTrainer(esConf, network, dataIter);
        EarlyStoppingResult<MultiLayerNetwork> result = trainer.fit();

        System.out.println(result);
        LOG.info("Termination reason: " + result.getTerminationReason());
        LOG.info("Termination details: " + result.getTerminationDetails());
        LOG.info("Total epochs: " + result.getTotalEpochs());
        LOG.info("Best epoch number: " + result.getBestModelEpoch());
        LOG.info("Score at best epoch: " + result.getBestModelScore());

        // Train with transformations
        for (ImageTransform transform : transforms) {
            System.out.print("\nTraining on transformation: " + transform.getClass().toString() + "\n\n");
            recordReader.initialize(trainData, transform);
            dataIter = new RecordReaderDataSetIterator(recordReader, batchSize, 1, numLabels);
            preProcessor.fit(dataIter);
            dataIter.setPreProcessor(preProcessor);
        }

        log.info("Evaluate model....");
        MultiLayerNetwork bestModel = (MultiLayerNetwork) result.getBestModel();
        network = bestModel;
        Evaluation eval = network.evaluate(testdataIter);
        log.info(eval.stats(true));

        log.info("Save model....");
        ModelSerializer.writeModel(network, modelOutputPath, true);
        log.info("**************** Classification finished ********************");
    }

    private ConvolutionLayer convInit(String name, int in, int out) {
        ConvolutionLayer inputLayer = new ConvolutionLayer.Builder(3, 3)
                .nIn(in)
                .nOut(out)
                .padding(0, 0)
                .stride(1, 1)
                .name(name)
                .biasInit(0)
                .build();
        return inputLayer;
    }

    private SubsamplingLayer maxPool(String name, int[] kernel) {
        SubsamplingLayer subsamplingLayer = new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                .kernelSize(kernel)
                .name(name)
                .build();
        return subsamplingLayer;
    }

    private ConvolutionLayer conv3x3(String name, int out) {
        ConvolutionLayer layer = new ConvolutionLayer.Builder(3, 3)
                .nOut(out)
                .padding(0, 0)
                .stride(1, 1)
                .name(name)
                .build();
        return layer;
    }

    public static void main(String[] args) throws Exception {
        new NetworkModel().execute(args);
    }
}



