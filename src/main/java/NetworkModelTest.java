import org.datavec.image.loader.NativeImageLoader;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.api.ops.impl.indexaccum.IAMax;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.nd4j.linalg.factory.Nd4j;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Scanner;

import static org.opencv.highgui.HighGui.imshow;
import static org.opencv.highgui.HighGui.waitKey;
import static org.opencv.imgproc.Imgproc.*;


public class NetworkModelTest {

    public static void main(String[] args) {
        String javaSavedModelPath = FileSystems.getDefault().getPath("src/main/resources/networkmodel.bin").toAbsolutePath().toString();
        String testImagePath = FileSystems.getDefault().getPath("src/main/resources/testimages/apple.jpg").toAbsolutePath().toString();
        String networkLabelsPath = FileSystems.getDefault().getPath("src/main/resources/networklabels.txt").toAbsolutePath().toString();
        nu.pattern.OpenCV.loadLocally();
        final MultiLayerNetwork multiLayerNetwork;

        try {
            multiLayerNetwork = ModelSerializer.restoreMultiLayerNetwork(javaSavedModelPath, true);
            Mat img = Imgcodecs.imread(testImagePath);
            Mat outputImg = new Mat();
            Size scaleSize = new Size(400, 400);
            Imgproc.resize(img, outputImg, scaleSize, 0, 0, INTER_LINEAR);
            NativeImageLoader loader = new NativeImageLoader(96, 96, 3);
            INDArray image = loader.asMatrix(img);
            ImagePreProcessingScaler preProcessor = new ImagePreProcessingScaler(0, 1);
            preProcessor.transform(image);
            INDArray output = multiLayerNetwork.output(image, false);
            System.out.println("output" + output);
            int outputLabelIndex = Nd4j.getExecutioner().execAndReturn(new IAMax(output)).getFinalResult().intValue();
            System.out.println("outputLabelIndex" + outputLabelIndex);
            ArrayList<String> labels = new ArrayList<>();
            try (Scanner in = new Scanner(new FileReader(networkLabelsPath))) {
                while (in.hasNext()) {
                    labels.add(in.nextLine());
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            String outputLabel = labels.get(outputLabelIndex);
            String outputValuePercentage = String.format("%.2f", output.getDouble(outputLabelIndex) * 100);
            String outputInfo = outputLabel + " : " + outputValuePercentage + '%';
            System.out.println(outputInfo);
            Imgproc.putText(outputImg, outputInfo, new Point(10, 25), FONT_HERSHEY_TRIPLEX, 0.7, new Scalar(0, 255, 0), 2);
            imshow("output", outputImg);
            waitKey(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
